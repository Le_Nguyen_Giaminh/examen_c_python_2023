#include <iostream>
#include <string>
#include <vector>
#include "film.h"

// Initialize sample data
std::vector<Film> initFilm() {
    // create a vector to stock films
    std::vector<Film> filmList;

    // Film data
    std::string names[] = {
        "Mad Max: Fury Road",
        "La La Land",
        "Parasite",
        "The Shape of Water",
        "Black Panther",
        "Joker",
        "Nomadland",
        "Spider-Man: Into the Spider-Verse",
        "1917",
        "Moonlight"
    };

    std::string directors[] = {
        "George Miller",
        "Damien Chazelle",
        "Bong Joon-ho",
        "Guillermo del Toro",
        "Ryan Coogler",
        "Todd Phillips",
        "Chloé Zhao",
        "Bob Persichetti, Peter Ramsey, Rodney Rothman",
        "Sam Mendes",
        "Barry Jenkins"
    };

    Genre genres[] = {
        Genre::ACTION,
        Genre::ROMANCE,
        Genre::DRAMA,
        Genre::FANTASY,
        Genre::ACTION,
        Genre::DRAMA,
        Genre::DRAMA,
        Genre::ANIMATION,
        Genre::WAR,
        Genre::DRAMA
    };

    Year years[] = {
        Year::YEAR_2015,
        Year::YEAR_2016,
        Year::YEAR_2019,
        Year::YEAR_2017,
        Year::YEAR_2018,
        Year::YEAR_2019,
        Year::YEAR_2020,
        Year::YEAR_2018,
        Year::YEAR_2019,
        Year::YEAR_2016
    };

    int ratings[] = {
        8,
        8,
        9,
        7,
        7,
        8,
        8,
        8,
        9,
        8
    };

    // Create film objects
    for (int i = 0; i < 10; ++i) {
        Film film(names[i], directors[i], genres[i], years[i], ratings[i]);
        filmList.push_back(film);
    }
}

// Function to search for a film by name
Film searchFilm(const std::string& name, std::vector<Film> filmList) {
    for (const auto& film : filmList) {
        if (film.getName() == name) {
            return film;
        }
    }
    std::cout << "Film not found." << std::endl;
}

// Function to check if a film exist in the database
bool checkIfExistFilm(const std::string& name, std::vector<Film> filmList) {
    for (const auto& film : filmList) {
        if (film.getName() == name) {   
            return true;
        }
    }
    return false;
}

// Function to add a film
std::vector<Film> addFilm(std::vector<Film> filmList) {
    std::string name;
    std::string director;
    Genre genre;
    Year year;
    int rating;

    std::cout << "Enter film tile: ";
    std::cin >> name;
    std::cout << "Enter film director: ";
    std::cin >> director;
    std::cout << "Enter film genre: ";
    std::string input;
    std::cin >> input;
    genre = convertStringToGenreEnum(input);
    std::cout << "Enter film year: ";
    std::cin >> input;
    year = convertStringToYearEnum(input);
    std::cout << "Enter film rating: ";
    std::cin >> rating;

    Film film(name, director, genre, year, rating);
    filmList.push_back(film);
    std::cout << "Film added successfully." << std::endl;
    return filmList;
}


// Function to delete a film by title
std::vector<Film> deleteFilm(std::vector<Film> filmList) {
    std::string name;
    std::cout << "Enter film tile: ";
    std::cin >> name;
    for (int i=0; i<filmList.size(); i++) {
        if (filmList.at(i).getName() == name) {
            //filmList.erase(i);
            std::cout << "Film has been deleted." << std::endl;
            return filmList;
        }
    }
    std::cout << "Film not found." << std::endl;
}

void describeFilm(std::vector<Film> filmList) {
   
}

Year convertStringToYearEnum(const std::string& yearStr) {
    if (yearStr == "2015")
        return Year::YEAR_2015;
    else if (yearStr == "2016")
        return Year::YEAR_2016;
    else if (yearStr == "2017")
        return Year::YEAR_2017;
    else if (yearStr == "2018")
        return Year::YEAR_2018;
    else if (yearStr == "2019")
        return Year::YEAR_2019;
    else if (yearStr == "2020")
        return Year::YEAR_2020;
    else if (yearStr == "2021")
        return Year::YEAR_2021;
    else if (yearStr == "2022")
        return Year::YEAR_2022;
    else if (yearStr == "2023")
        return Year::YEAR_2023;
    return Year::INVALID_YEAR;
}

Genre convertStringToGenreEnum(const std::string& genreStr) {
    if (genreStr == "ACTION")
        return Genre::ACTION;
    else if (genreStr == "ROMANCE")
        return Genre::ROMANCE;
    else if (genreStr == "DRAMA")
        return Genre::DRAMA;
    else if (genreStr == "FANTASY")
        return Genre::FANTASY;
    else if (genreStr == "ANIMATION")
        return Genre::ANIMATION;
    else if (genreStr == "WAR")
        return Genre::WAR;
    return Genre::INVALID_GENRE;
}