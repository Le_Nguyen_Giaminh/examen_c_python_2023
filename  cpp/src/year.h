#ifndef YEAR_H
#define YEAR_H

enum class Year {
    YEAR_2015,
    YEAR_2016,
    YEAR_2017,
    YEAR_2018,
    YEAR_2019,
    YEAR_2020,
    YEAR_2021,
    YEAR_2022,
    YEAR_2023,
    INVALID_YEAR
};

#endif
