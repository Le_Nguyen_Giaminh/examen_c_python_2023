#include <iostream>
#include <string>
#include <vector>
#include "film.h"
#include "operations.cpp"

// Global variables
std::vector<Film> filmList;

int choice;

int main() {
    std::cout << "----Welcome to Film Database----\n";
    filmList = initFilm();
    
    do {
        std::cout << "Menu:\n";
        std::cout << "1. Add film\n";
        std::cout << "2. Delete film\n";
        std::cout << "3. Display film details\n";
        std::cout << "4. Exit\n";
        std::cout << "Enter your choice: ";
        std::cin >> choice;

        switch (choice) {
            case 1: {
                filmList = addFilm(filmList);
                break;
            }
            case 2: {
                filmList = deleteFilm(filmList);
            }
            case 3: {
                describeFilm(filmList);
            }
            case 4: {
                std::cout << "Exiting the application.\n";
                break;
            }
        }
    } while (choice != 4);
} 
