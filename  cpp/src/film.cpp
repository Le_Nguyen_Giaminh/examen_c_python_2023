#include "film.h"
#include "genre.h"
#include "year.h"

Film::Film(const std::string& name, const std::string& director, const Genre genre, const Year year, const int rating): 
name(name), director(director), genre(genre), year(year), rating(rating) {}

// Getter functions
std::string Film::getName() const {
    return name;
}

std::string Film::getDirector() const {
    return director;
}

Genre Film::getGenre() const {
    return genre;
}

Year Film::getYear() const {
    return year;
}

int Film::getRating() const {
    return rating;
}

// Modifier functions
void Film::modifyName(const std::string& newName) {
    name = newName;
}

void Film::modifyDirector(const std::string& newDirector) {
    director = newDirector;
}

void Film::modifyGenre(Genre newGenre) {
    genre = newGenre;
}

void Film::modifyYear(Year newYear) {
    year = newYear;
}

void Film::modifyRating(const int& newRating) {
    rating = newRating;
}