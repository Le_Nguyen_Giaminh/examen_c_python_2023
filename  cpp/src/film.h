#ifndef FILM_H
#define FILM_H

#include <string>
#include "genre.h"
#include "year.h"

class Film {
    private:
        std::string name;
        std::string director;
        Genre genre;
        Year year;
        int rating;

    public:
        // Constructor
        Film(const std::string& name, const std::string& director, const Genre genre, const Year year, const int rating);

        // Getter functions
        std::string getName() const;
        std::string getDirector() const;
        Genre getGenre() const;
        Year getYear() const;
        int getRating() const;

        // Modifier functions
        void modifyName(const std::string& newName);
        void modifyDirector(const std::string& newDirector);
        void modifyGenre(Genre newGenre);
        void modifyYear(Year newYear);
        void modifyRating(const int& newRating);
};

#endif 
