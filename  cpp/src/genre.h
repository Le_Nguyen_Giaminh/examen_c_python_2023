#ifndef GENRE_H
#define GENRE_H

enum class Genre {
    ACTION,
    ROMANCE,
    DRAMA,
    FANTASY,
    ANIMATION,
    WAR,
    INVALID_GENRE
};

#endif