from player import Player

class Thief(Player):
    def __init__(self, name):
        super().__init__(name, health=70, attack_power=35, defense=3)