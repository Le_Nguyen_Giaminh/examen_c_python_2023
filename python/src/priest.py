from player import Player

class Priest(Player):
    def __init__(self, name):
        super().__init__(name, health=60, attack_power=15, defense=12)