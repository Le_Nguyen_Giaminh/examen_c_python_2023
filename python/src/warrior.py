from player import Player

class Warrior(Player):
    def __init__(self, name):
        super().__init__(name, health=100, attack_power=20, defense=10)
