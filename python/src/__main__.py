from thief import Thief
from archer import Archer
from mage import Mage
from priest import Priest
from warrior import Warrior
from player import Player
import random

def choose_class():
    print("Choose your class:")
    print("1. Warrior")
    print("2. Mage")
    print("3. Archer")
    print("4. Thief")
    print("5. Priest")

    while True:
        choice = input("Enter the number corresponding to your class: ")

        if choice == "1":
            return Warrior("Warrior")
        elif choice == "2":
            return Mage("Mage")
        elif choice == "3":
            return Archer("Archer")
        elif choice == "4":
            return Thief("Thief")
        elif choice == "5":
            return Priest("Priest")
        else:
            print("Invalid choice. Try again.")


def battle(player, enemy):
    print(f"A wild {enemy.name} appears!")

    while player.is_alive() and enemy.is_alive():
        player.attack(enemy)

        if enemy.is_alive():
            enemy.attack(player)

    if player.is_alive():
        print(f"{player.name} wins the battle!")
    else:
        print("You have been defeated.")


def main():
    print("Welcome to the RPG game!")

    player = choose_class()

    enemy = Player("Enemy", health=random.randint(50, 100), attack_power=random.randint(10, 30), defense=random.randint(1, 10))

    battle(player, enemy)


if __name__ == "__main__":
    main()
