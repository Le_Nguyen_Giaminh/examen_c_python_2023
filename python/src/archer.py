from player import Player

class Archer(Player):
    def __init__(self, name):
        super().__init__(name, health=90, attack_power=25, defense=8)