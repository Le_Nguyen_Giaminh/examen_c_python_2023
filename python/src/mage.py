from player import Player

class Mage(Player):
    def __init__(self, name):
        super().__init__(name, health=80, attack_power=30, defense=5)